#!/usr/bin/python3
import traceback
import sys
import inspect
from importlib import reload
from ogame import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def maxEvolv(planet, menu, buildings, wait='0s', halfEvolv=False):
    volta = 0
    prev = []
    latency = 1
    wait = stringToSeconds(wait)
    print('waiting', wait)
    sleep(wait)
    print('starting')
    while True:
        try:
            global driver
            driver = Ogame(latency)
            hasResorces = True
            while hasResorces:
                hasResorces = False
                for i in buildings:
                    driver.changePlanet(planet)
                    driver.changeMenu(menu)
                    if volta % 2 == 0 and (i == 3 or i == 5) and halfEvolv:
                        continue

                    det = driver.selectBuilding(i)
                    if driver.evolve():
                        print('sleeping for ', det['Tempo'])
                        sleep(det['Tempo']+3)
                        return True

                # driver.scanResources()
                # driver.sendResources(planet, driver.resources[0][:-1]+[0], human=False)
                volta += 1
            break

        except KeyboardInterrupt:
            print('Ending')
            return

        except:
            print(traceback.format_exc())
            a = datetime.now()
            print('error' + str(a))
            prev += [a]
            if len(prev) > 5:
                prev = prev[1:]
            elif prev[-1] - prev[0] < timedelta(minutes=1):
                sleep(60)


def farm():
    while True:
        try:
            global driver
            driver = Ogame()
            #driver.slots = 20
            while True:
                driver.getArrivalInfo()
                available = driver.slots - len(driver.arrival)
                if 6 >= available:
                    print('waiting for ', 6 - available)
                    driver.waitFlight(6 - available)
                print('wait for flight to be returning')
                sleep(driver.flightReturningToSeconds())

                # driver.getOffer()
                driver.scanResources()

                resources = [sum(e) for e in driver.resources]
                m = resources.index(min(resources))

                driver.farmInactives(m)

        except KeyboardInterrupt:
            print('Ending')
            return

        except Exception as e:
            traceback.print_exc()
            print(datetime.now())
            try:
                driver.driver.close()
            except:
                pass
            sleep(60)


def intInput(message, default=None):
    i = input(message)
    if default != None and i == '':
        return default

    elif default == None and i == '':
        print('Error!')
        return -1
    else:
        return int(i)


def printSource(function):
    print(inspect.getsource(function))

def sendResources(myPlanet=True):
    toPlanet = None
    if myPlanet:
        toPlanet = intInput('Planet To?')
    else:
        toPlanet = stringToCoord(input('coord string to parse?'))

    metal = intInput('metal?   ', 0) * 1000
    cristal = intInput('cristal? ', 0) * 1000
    deuterio = intInput('deuterio?', 0) * 1000
    moon = input('moon?') == 'y'
    mode = input('mode?') == 'y'
    if mode:
        mode = '4' # transfer
    else:
        mode = '3' # transport

    ban = []
    new = toPlanet
    while new != '':
        ban += [new]
        new = intInput('ban planet?', '')

    if toPlanet == -1 or metal == -1 or cristal == -1 or deuterio == -1:
        return

    driver.sendResources(toPlanet, [metal, cristal, deuterio], moon, ban, mode=mode)


def farmInactives():
    fromPlanet = intInput('Planet From?')
    scan = input('scan?') == 'y'
    index = intInput('restart index?', 0)
    driver.farmInactives(fromPlanet, scan, index)


def getResources():
    driver.getResources(input('new Scan?') != 'n')


def reloadCode():
    global driver
    driver = reload(sys.modules['ogame']).Ogame(driver)

if __name__ == '__main__':
    driver = Ogame()
