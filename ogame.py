#!/usr/bin/python3
from datetime import datetime
from datetime import timedelta
from time import sleep
from time import time
from bs4 import BeautifulSoup
import json
import os
import re
import progressbar
import math
import requests

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException


class Ogame():
    def __init__(self, ogame=None):
        self.universeFlightAcceleration = 3
        self.bigCapacity = 45000
        self.banned = []
        self.arrival = []
        self.wait = 5
        self.velocity = {'spy' : (10**8, 0, 0), 'bigCargo':(7500, 0, 1)}
        self.baseURL = "https://s145-pt.ogame.gameforge.com/api/"

        if ogame != None:
            self.driver = ogame.getDriver()
            self.slots = ogame.slots
            self.coord = ogame.coord
            self.loadData()

        else:
            self.login()

    def login(self):
        option = webdriver.ChromeOptions()
        if os.uname().sysname == 'Linux':
            option.binary_location = "/usr/bin/firefox"
        else:
            option.binary_location = "/Applications/Brave Browser.app/Contents/MacOS/Brave Browser"

        option.add_experimental_option("useAutomationExtension", False)
        # options=option
        option.add_experimental_option("excludeSwitches",["enable-automation"])
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()
        self.driver.get("https://lobby.ogame.gameforge.com/")
        self.driver.add_cookie(
            {'name': 'gf-token-production', 'value': '4f2c5634-3db8-476e-b535-07ac8d434505'})
        self.driver.get("https://lobby.ogame.gameforge.com/")

        # close cookies popup
        self.clickXPath('//span[@class="cookiebanner4"]/button[2]')
        self.clickXPath('//*[@id="joinGame"]/button')

        # switch to game tab
        self.driver.switch_to.window(self.driver.window_handles[1])

        # load game info
        planets = self.waitAllXPathVisible(
            '//*[@id="planetList"]/div/a/span[2]')
        self.coord = [list(map(int, i.text[1:-1].split(':'))) for i in planets]
        # self.banned = [self.coord[-1]]
        self.loadData()
        self.slots = int(self.buildings[0][2][9])+1
        #self.slots = 20

        # close login tab
        self.driver.switch_to.window(self.driver.window_handles[0])
        self.driver.close()
        self.driver.switch_to.window(self.driver.window_handles[0])

        # game bug where at login shows wrong planet selected
        self.clickXPath('//*[@id="planetList"]/div[1]/a[1]')

    def __enter__(self):
        return Ogame()
    def __exit__(self, type, value, traceback):
        print(value)
        print(traceback)
        self.driver.close()

    def getDriver(self):
        return self.driver

    def findXPath(self, string):
        return self.driver.find_element_by_xpath(string)

    def findAllXPath(self, string):
        return self.driver.find_elements_by_xpath(string)

    def findCssSelector(self, string):
        return self.driver.find_element_by_css_selector(string)

    def findAllCssSelector(self, string):
        return self.driver.find_elements_by_css_selector(string)

    def myWait(self, method):
        try:
            return WebDriverWait(self.driver, self.wait).until(method)
        
        except:
            sleep(1)
            return WebDriverWait(self.driver, self.wait).until(method)

    def waitXPathVisible(self, path):
        return self.myWait(expected_conditions.
                visibility_of_element_located((By.XPATH, path)))

    def waitAllXPathVisible(self, path):
        return self.myWait(expected_conditions.
                visibility_of_all_elements_located((By.XPATH, path)))

    def waitXPathClickable(self, path):
        return self.myWait(expected_conditions.
                element_to_be_clickable((By.XPATH, path)))

    def waitXPathNotVisible(self, path):
        return self.myWait(expected_conditions.
                invisibility_of_element((By.XPATH, path)))

    def waitXPathText(self, path, string):
        return self.myWait(expected_conditions.
                text_to_be_present_in_element((By.XPATH, path), string))

    def waitXPathExist(self, path):
        return self.myWait(expected_conditions.
                presence_of_element_located((By.XPATH, path)))

    def getXPathText(self, string):
        return self.waitXPathVisible(string).text

    def getXPathAttribute(self, xpath, attribute):
        return self.waitXPathExist(xpath).get_attribute(attribute)

    def clickXPath(self, xpath):
        self.waitXPathClickable(xpath).click()

    def sendKeysXPath(self, xpath, keys):
        self.waitXPathVisible(xpath).send_keys(keys)

    def changePlanet(self, planet, moon=False):
        p = None
        if not moon:
            p = self.waitXPathVisible(
                '//*[@id="planetList"]/div['+str(planet)+']/a[1]')
        else:
            p = self.waitXPathVisible(
                '//*[@id="planetList"]/div['+str(planet)+']/a[2]')

        if (p != self.findCssSelector('#planetList div .active')):
            p.click()

    def changeMenu(self, menu):
        m = self.waitXPathVisible('//*[@id="menuTable"]/li['+str(menu)+']/a')
        try:
            if m != self.findCssSelector('#menuTable li .selected'):
                m.click()
        except:
            m.click()

    def selectBuilding(self, building):
        listSelected = self.findAllCssSelector('.showsDetails')
        element = self.waitXPathClickable(
            '//*[@id="technologies"]/ul/li['+str(building)+']')
        if not listSelected or listSelected[0] != element:
            element.click()

        # wait for details to load
        self.waitXPathNotVisible('//*[@id="technologydetails_content"]/div[1]')
        time = stringToSeconds(self.getXPathText(
            '//*[@id="technologydetails"]/div[2]/div/ul/li/time'))
        resources = self.findAllXPath(
            '//*[@id="technologydetails"]/div[2]/div/div[1]/ul/li')
        details = {'Tempo': time, 'Metal': 0,
                   'Cristal': 0, 'Deutério': 0, 'Energia': 0}
        for r in resources:
            data = r.get_attribute('aria-label').split(" ")
            details[data[1]] = self.stringToNumber(data[0])
        return details

    def evolve(self, building, planet=None, menu=None):
        if planet:
            self.changePlanet(planet)
        if menu:
            self.changeMenu(menu)
        details = self.selectBuilding(building)
        if self.isEnough(self.getPlanetResources(),
                             [details['Metal'], details['Cristal'], details['Deutério']]):
            self.clickXPath('//*[@class="upgrade"]')
            if int(menu) == 2 and planet != None:
                self.buildings[planet - 1][0][building - 1] += 1
                self.saveBuildings()
            return True
        return False

    def isEnough(self, has, goal):
        return has[0] >= goal[0] and has[1] >= goal[1] and has[2] >= goal[2]

    def hasResorces(self, goal):
        return self.isEnough(self.resources[0],goal)

    def getResources(self, scan=False):
        if not hasattr(self, 'resources') or scan:
            self.scanResources()

        print('\n\t'.join(';  '.join(self.numberToString(rec)
                                     for rec in pla) for pla in self.resources))

    def getPlanetResources(self):
        return [self.stringToNumber(self.getXPathText(
            '//*[@id="resources"]/li['+str(i)+']')) for i in range(1, 5)]

    def scanResources(self, p=False):
        self.numPlanets = len(self.driver.find_elements_by_xpath(
            '//*[@id="planetList"]/div'))
        self.resources = []
        for i in range(1, self.numPlanets+1):
            self.changePlanet(i)
            self.resources += [self.getPlanetResources()]

        s = [[sum([planet[t] for planet in self.resources]) for t in range(3)]]
        self.resources = s + self.resources

    def sendShips(self, planteFrom, coord, resources, moon, mode):
        self.changePlanet(planteFrom)
        self.changeMenu(8)

        num = (sum(resources) // self.bigCapacity) + 1

        # wait second part to load
        self.waitXPathVisible('//div[@id="fleet1"]//h2')
        self.sendKeysXPath(
            '//*[@id="civil"]/li[2]/input', str(num) + Keys.RETURN)

        # wait second part to load
        self.waitXPathVisible('//div[@id="fleet2"]//h2')
        if moon:
            self.clickXPath('//*[@id="mbutton"]')

        self.sendKeysXPath('//*[@id="galaxy"]', coord[0])
        self.sendKeysXPath('//*[@id="system"]', coord[1])
        self.sendKeysXPath('//*[@id="position"]', str(coord[2]) + Keys.RETURN)

        self.clickXPath('//*[@id="missionButton'+mode+'"]')
        if mode == '3' or mode == '4':
            self.sendKeysXPath('//*[@id="metal"]', resources[0])
            self.sendKeysXPath('//*[@id="crystal"]', resources[1])
            self.sendKeysXPath('//*[@id="deuterium"]', str(resources[2]))

        # read what will be sent
        rec = ['metal', 'crystal', 'deuterium']
        sent = [self.stringToNumber(self.getXPathAttribute(
            '//*[@id="' + e + '"]', 'value')) for e in rec]
        if (mode == '3' or mode == '4'):
            pass#print(sent)

        self.clickXPath('//*[@id="sendFleet"]')
        # wait for page to load
        self.waitXPathVisible('//*[@id="fleet1"]//h2')
        self.addBigCargoFlight(self.coord[planteFrom - 1], coord)
        return sent

    def sendResources(self, planet, sendResources, moon=False, filt=[], human=True, mode='3'):
        # resources minimum resources to keep on the planet
        margin = 15000
        self.scanResources()

        if not self.hasResorces(sendResources):
            print('not enough Resources!')
            return

        filt = [self.coord[e-1] for e in filt] + self.banned
        total = [0, 0, 0]
        sentTotal = [0, 0, 0]
        if isinstance(planet, int):
            total = self.resources[planet][:-1]  # last is energy
            sentTotal = total[:]
            planet = self.coord[planet - 1]  # starts at zero

        l = list(filter(lambda x: x != planet and x not in filt, self.coord))
        l.sort(key=lambda x: sum(
            self.resources[self.coord.index(x)+1][:-1]), reverse=True)
        l.sort(key=lambda x: abs(x[1] - planet[1]))
        l.sort(key=lambda x: abs(x[0] - planet[0]))

        orderd = [self.coord.index(e) + 1 for e in l]
        send = []

        for p in orderd:
            done = True
            aux = []
            for i in range(len(sendResources)):
                aux += [min(
                        (self.resources[p][i]-margin+10000)//10000*10000,
                        max(0, sendResources[i]-total[i])
                        )]
                total[i] += aux[i]
                if total[i] < sendResources[i]:
                    done = False

            send += [aux]
            if done:
                break

        if human:
            print(''.join(str(orderd[i]) + '->' +
                          str(send[i])+';\n' for i in range(len(send))))

        for i in range(len(send)):
            sent = self.sendShips(orderd[i], planet, send[i], moon, mode)
            for i in range(3):
                sentTotal[i] += sent[i]

        missing = []
        for i in range(3):
            if sentTotal[i] < sendResources[i]:
                missing += [sendResources[i] - sentTotal[i]]
            else:
                missing += [0]

        print(missing)
        return missing

    def numberToString(self, i):
        return '\'' + '{:,}'.format(i).replace(',', ' ') + '\''

    def stringToNumber(self, string):
        string = string.replace('.', '')
        if string.find(','):
            string = string.replace('M', '0'*3).replace(',', '')
        else:
            string = string.replace('M', '0'*6)
        return eval(string)

    def filterNonEmptyReports(self):
        return list(filter(lambda x: x[2] == 0 and x[3] == 0, self.reports))

    def farmInactives(self, fromPlanet, scanAll=False, restart=0):
        self.changePlanet(fromPlanet)
        self.getArrivalInfo()

        if restart == 0:
            self.spyInactives(fromPlanet, scanAll)

        elif restart == -1:
            restart = 0
            
        self.reports = list(filter(
                lambda x: x[0] > 300000,
                self.filterInactiveReports()))
        self.saveData()
        reports = self.filterNonEmptyReports()
        reports.sort(key=lambda x:x[0], reverse=True)

        while self.waitSlot(most=60*3) and restart < len(reports):
            self.sendShips(
                    fromPlanet,
                    reports[restart][1],
                    [int(reports[restart][0]*1.1)],
                    False,
                    '1')
            restart += 1

    def readReports(self):
        ret = []
        for e in self.findAllXPath('//*[@id="fleetsgenericpage"]/ul/li'):
            text = e.get_attribute('innerHTML')
            coord = re.search('\[.*:.*:.*\]', text)
            resources = re.search('Recu.{0,30}</', text)
            fleet = re.search('>Frota.{0,30}</', text)
            defense = re.search('Defesa.{0,30}</', text)

            if coord == None or resources == None or defense == None or fleet == None:
                self.filterStatistics["noInfo"] += 1
                continue
            
            coord = tuple(map(int, coord.group()[1: -1].split(':')))
            resources = resources.group()[10: -9]
            fleet = fleet.group()[9: -2]
            defense = defense.group()[9: -9]

            if fleet != '0' or defense != '0':
                self.filterStatistics["notEmpty"] += 1

            resources = self.stringToNumber(resources)
            fleet = self.stringToNumber(fleet)
            defense = self.stringToNumber(defense)
            if resources > 150000:
                ret += [[int(resources*0.6), coord, fleet, defense]]
        return ret

    def filterInactiveReports(self):
        self.clickXPath('//*[@id="message-wrapper"]/a[1]')
        self.filterStatistics = {"noInfo":0, "notEmpty":0, "bug?":0}
        pagination = ['0', 0]
        reports = []
        while pagination[0] != pagination[1]:
            paginationXPath = '//*[@id="fleetsgenericpage"]/ul/ul[1]/li[3]'
            self.waitXPathText(paginationXPath, str(
                int(pagination[0]) + 1)+'/')
            pagination = self.getXPathText(paginationXPath).split('/')

            reports += self.readReports()
            self.clickXPath('//*[@id="fleetsgenericpage"]/ul/ul[1]/li[4]')

        reports.sort(key=lambda x: x[0], reverse=True)
        return reports

    def waitSlot(self, most=0):
        if len(self.arrival) >= self.slots:
            return self.waitFlight(0, most=most)
        return True

    def waitArrival(self, most=None):
        if len(self.arrival) == 0:
            return

        most = self.now() + timedelta(minutes=most if most !=None else 4)

        sec = lambda :(most - self.now()).total_seconds()
        while self.waitFlight(0, sec()) and len(self.arrival) != 0:
            pass
        
    def waitFlight(self, index, most=0):
        if len(self.arrival) == 0: return True

        self.arrival.sort()
        delta = int((self.arrival[index] - self.now()).total_seconds())

        if delta > most and most != 0: return False

        if (delta > 0):
            sleep(delta+3)

        now = self.now()
        self.arrival = list(filter(lambda x: x >= now, self.arrival))
        return True

    def spyInactives(self, fromPlanet, scan=False):
        # delete old reports
        self.clickXPath('//*[@id="message-wrapper"]/a[1]')
        self.clickXPath('//*[@id="subtabs-nfFleetTrash"]/div/span[1]/span')

        # wait messages deletion
        # self.waitXPathNotVisible('//*[@id="fleetsgenericpage"]/ul/ul[1]/li[3]')
        self.waitXPathVisible('//*[@id="fleetsgenericpage"]/ul/ul[1]/li[3]')

        self.changeMenu(9)
        self.slots = int(self.getXPathText('//*[@id="slotValue"]').split('/')[-1])
        if scan:
            self.spyInactivesTargets(fromPlanet, self.inactive)
        else:
            self.reports.sort()
            self.spyInactivesTargets(fromPlanet, list(map(
                    lambda x: x[1],
                    filter(lambda x: x[2]==0 and x[3]==0, self.reports))))

    def calcDistance(self, coord1, coord2, limit=[5, 499, 15], a=[20000, 95, 5], b=[0, 2700, 1000]):
        #fixed value 5
        if 0 == len(coord1): return 5

        if coord1[0] != coord2[0]:
            limit = [5, 499, 15]
            a = [20000, 95, 5]
            b = [0, 2700, 1000]

            aux = abs(coord2[0] - coord1[0])
            i = len(coord1)
            return b[-i] + a[-i] * (aux if aux <= limit[-i]/2 else limit[-i] - aux)
        
        else:
            return self.calcDistance(coord1[1:], coord2[1:], limit[1:], a[1:], b[1:])
    
    def calcVelocity(self, ship):
        # spy ship hard coded
        vel, engin, bonus = self.velocity[ship]
        return vel/10 * self.buildings[0][2][5 + engin] + vel + vel*bonus

    def calcFlight(self, coord1, coord2, slowestShip, speed=1):
        def calculate():
            return round((10 + 3500/speed * math.sqrt(10*distance/velocity)) 
                            / self.universeFlightAcceleration)
            
        distance = self.calcDistance(coord1, coord2)
        velocity = self.calcVelocity(slowestShip)
        return calculate()
    
    def addSpyFlight(self,coord1, coord2):
        self.arrival += [
            self.now() 
            + timedelta(seconds=2 * self.calcFlight(coord1, coord2, 'spy'))]

    def addBigCargoFlight(self,coord1, coord2):
        self.arrival += [
            self.now() 
            + timedelta(seconds=2 * self.calcFlight(coord1, coord2, 'bigCargo'))]

    def spyInactivesTargets(self, fromPlanet, targets):
        fromPlanet = self.coord[fromPlanet - 1]
        targets.sort()
        bar = progressBar(len(targets))
        bar.start()
        for j in range(len(targets)):
            bar.update(j)
            coord = targets[j]
            
            isNotGalaxy = str(coord[0]) != self.getXPathAttribute('//*[@id="galaxy_input"]', 'value')
            isNotSystem = str(coord[1]) != self.getXPathAttribute('//*[@id="system_input"]', 'value')
            # change viewed system
            if isNotGalaxy or isNotSystem:
                if isNotGalaxy: self.sendKeysXPath('//*[@id="galaxy_input"]', coord[0])
                if isNotSystem: self.sendKeysXPath('//*[@id="system_input"]', coord[1])
                self.clickXPath('//*[@id="galaxyHeader"]/form/div[1]')
                self.waitXPathNotVisible('//*[@id="galaxyLoading"]/img')

            # send ships
            self.waitSlot()

            try:
                spy = self.findXPath(
                    '//*[@id="galaxytable"]/tbody/tr['+str(coord[2])+']/td[8]/span/a[1]/span')
                if spy in self.findAllCssSelector('.inactive_filter .icon_eye'):
                    spy.click()
                    self.addSpyFlight(fromPlanet, coord)
                    sleep(2)

            except NoSuchElementException:
                print('error ' + str(coord))
            
            except ElementClickInterceptedException:
                print('Not supososed to happen')

        self.waitArrival()
        bar.finish()

    def getFullDetails(self):
        def extractMenu(string, m):
            self.changeMenu(m)
            ret = []
            for e in self.waitAllXPathVisible(string):
                ret += [self.stringToNumber(e.text)]
            return [ret]

        self.buildings = []

        # research is account wide
        research = extractMenu('//*[@id="technologies"]/div/ul/li/span/span/span[1]', 5)

        for planet in range(len(self.coord)):
            self.buildings += [[]]
            self.changePlanet(planet+1)
            self.buildings[-1] += extractMenu('//*[@id="technologies"]/ul/li/span/span/span[1]', 2)

            self.buildings[-1] += extractMenu('//*[@id="technologies"]/ul/li/span/span/span[1]', 3)

            self.buildings[-1] += research

            self.buildings[-1] += extractMenu('//*[@id="technologies"]/div/ul/li/span/span/span[1]', 6)

            self.buildings[-1] += extractMenu('//*[@id="technologies"]/ul/li/span/span/span[1]', 7)

    def getMineProduction(self):
        self.clickXPath('//*[@id="menuTable"]/li[2]/span/a')
        self.production = []
        for i in range(1, len(self.coord)+1):
            self.changePlanet(i)
            self.production += [[]]
            for j in range(2, 5):
                self.production[-1] += [self.stringToNumber(self.getXPathText(
                    '//*[@id="inhalt"]/div[2]/div[2]/form/table/tbody/tr[18]/td['+str(j)+']/span'))]

        self.production = [[sum([planet[t] for planet in self.production])
                            for t in range(3)]] + self.production
        print(';  '.join(self.numberToString(rec*24)
                         for rec in self.production[0]))
        print('\n\t'.join(';  '.join(self.numberToString(rec)
                                     for rec in pla) for pla in self.production))
        self.changeMenu(1)

    def saveData(self):
        data = [self.buildings] + [self.production] + [self.inactive] + [self.reports]
        with open('data.json', 'w') as file:
            json.dump(data, file)

    def saveBuildings(self):
        b = self.buildings
        i = self.inactive
        self.loadData()
        data = [b] + [self.production] + [self.inactive]
        with open('data.json', 'w') as file:
            json.dump(data, file)
        self.buildings = b
        self.inactive = i

    def loadData(self):
        with open('data.json') as file:
            data = json.load(file)
            self.buildings = data[0]
            self.production = data[1]
            self.inactive = data[2]
            self.reports = data[3]

    def coordFromPlanet(self, planet):
        return ':'.join(map(str, self.coord[planet-1]))

    def flightReturningToSeconds(self):
        flights = [e.get_attribute('innerHTML') for e in self.findAllXPath(
            '//tr[@data-return-flight="false"]/td[1]/span')]

        if flights and flights[-1] != 'Concluído':
            return stringToSeconds(flights[-1])
        else:
            return 0

    def getArrivalInfo(self):
        flights = [e.get_attribute('innerHTML') for e in self.findAllXPath(
            '//tr[@data-return-flight="true"]/td[1]/span')]
        self.arrival = [self.now() + timedelta(seconds=stringToSeconds(e))
                        for e in flights if e != 'Concluído']

    def now(self):
        text = self.getXPathText('//*[@id="bar"]/ul/li[9]').split(' ')
        date = [int(e) for e in text[0].split('.')]
        time = [int(e) for e in text[1].split(':')]
        return datetime(day=date[0], month=date[1], year=date[2], hour=time[0], minute=time[1], second=time[2])

    def getOffer(self):
        try:
            self.driver.get(
                'https://s145-pt.ogame.gameforge.com/game/index.php?page=ingame&component=traderOverview#animation=false&page=traderImportExport')
            self.clickXPath(
                '//*[@id="div_traderImportExport"]/div[2]/div[2]/div[2]/div[2]/div/table/tbody/tr[1]/td[5]/a')
            self.clickXPath(
                '//*[@id="div_traderImportExport"]/div[2]/div[2]/div[2]/div[2]/a')
            self.clickXPath(
                '//*[@id="div_traderImportExport"]/div[2]/div[2]/div[2]/div[1]/a[2]')
        except:
            pass

    def getDetails(self, menu, building):
        return [e[menu][building] for e in self.buildings]
    
    def energyFromFusion(self, plant, technology):
        return 30 * plant * (1.05 + technology * 0.01) ** plant
    
    def getInactivePlayers(self):
        self.inactive = []
        response = requests.get(self.baseURL+'players.xml')
        soap = BeautifulSoup(response.text, 'html.parser')
        inactives = soap.find_all(status='i') + soap.find_all(status='I')

        bar = progressBar(len(inactives))
        bar.start()
        count = 0
        for i in map(lambda x: x.get('id'), inactives):
            bar.update(count)
            count += 1

            response = requests.get(self.baseURL+'playerData.xml?id='+str(i))
            soap = BeautifulSoup(response.text, 'html.parser')
            self.inactive += list(map(
                    lambda x: tuple(map(int, x.get('coords').split(':'))),
                    soap.find_all('planet')))

        bar.finish()
        self.inactive.sort()
        self.saveData()

def stringToSeconds(string):
    timeMark = {'d': '*60*60*24', 'h': '*60*60', 'm': '*60', 's': '', ' ': '+'}
    for l in timeMark:
        if l in string:
            string = string.replace(l, timeMark[l])
    return eval(string)


def stringToCoord(string, s=':'):
    return string.split(s)

def progressBar(m):
    return progressbar.ProgressBar(maxval=m, widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
